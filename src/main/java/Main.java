import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {


        ExecutorService executorService = Executors.newFixedThreadPool(3);
        HttpClient httpClient = HttpClient.newHttpClient();
        try {
            FileOutputStream os = new FileOutputStream("out.txt");
             os.flush();
        } catch (IOException  e ) {
            e.printStackTrace();
        }


        try (InputStream is = new FileInputStream("listURLs.txt")) {
            Scanner scanner = new Scanner(is);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().trim();
                executorService.submit(() -> {
                    System.out.println("!!! " + Thread.currentThread().getName());
                    String headers = getHeaders(httpClient, line);
                    writeToFile(line, headers);
                });
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
        System.out.println("finish " + Thread.currentThread().getName());


    }


    public static String getHeaders(HttpClient httpClient, String url) {
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();

        try {

            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            //System.out.println(response.statusCode());
           // System.out.println(response.headers().toString());

            System.out.println(response.headers().map().toString());
            return response.headers().map().toString();
            //System.out.println(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return "";

    }

     synchronized   public static void writeToFile(String url, String headers) {
         try (OutputStream os = new FileOutputStream("out.txt",true)) {


            os.write((url + "\r\n").getBytes(StandardCharsets.UTF_8));
            os.write((headers + "\r\n").getBytes(StandardCharsets.UTF_8));
            os.write("\r\n".getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

